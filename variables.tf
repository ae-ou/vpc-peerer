variable "requestor_vpc" {
    type = string
    description = "The VPC from which we will be sending the peering requests"
}

variable "requestee_vpcs" {
    type = list(
        object({
            create_route_rule = bool
            peer_region = string
            peer_vpc = string
            auto_accept_request = bool
            remote_vpc_dns_resolution = bool
            classic_link_to_remote_vpc = bool
            vpc_to_remote_classic_link = bool
            tags = list(string)
        })
    )
    description = "A list of objects that describe the peering connections"
}