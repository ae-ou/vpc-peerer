#VPC Peerer
A Terraform module used to create peering connections between AWS VPCs

##Notes
- The AWS provider is passed implicitly, as it is assumed that the primary VPC (i.e. the VPC requesting the peering connection) is created by the parent module - meaning that the provider configuration would be unchanged in this module.
- This module only covers the requester's side of the connection using `aws_vpc_peering_connection` - accepting the connection is not covered by this module unless the two VPCs are in the same account, and the `auto_accept` config is equal to `true`.

## Additional Info
- https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html
- https://www.terraform.io/docs/providers/aws/r/vpc_peering_connection.html#allow_classic_link_to_remote_vpc
